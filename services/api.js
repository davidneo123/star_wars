let XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;

exports.getSW=(url)=>{
  
    return new Promise((resolve, reject) => {
      var req = new XMLHttpRequest();
      req.open('GET', url);
      req.onload = function() {
        if (req.status == 200) {
          resolve(req.responseText);
        }
        else {
          reject(Error(req.statusText));
        }
      }
   
      req.onerror = function() {
        reject(Error("Network Error"));
      }
  
      req.send()
     
    })
  }
